package com.example.instamoodindex.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.instamoodindex.ViewModel.ChangeBoardClass.BoardRepository
import com.example.instamoodindex.ViewModel.ChangeBoardClass.BoardViewModel
import com.example.instamoodindex.ViewModel.ChangeBoardClass.BoardViewModelFactory
import com.example.instamoodindex.adapters.BoardAdapter
import com.example.instamoodindex.base.BaseFragment
import com.example.instamoodindex.databinding.FragmentChangeBoardClassBinding
import com.example.instamoodindex.helper.Resource
import com.example.instamoodindex.retrofit.RetrofitService


class ChangeBoardClassFragment : BaseFragment() {
    private lateinit var binding: FragmentChangeBoardClassBinding
    private lateinit var viewModel: BoardViewModel
    private val retrofitService = RetrofitService.getInstance()
    private val adapter = BoardAdapter()

    override fun initView(layoutInflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentChangeBoardClassBinding.inflate(layoutInflater, container, false)
        viewModel =
            ViewModelProvider(this, BoardViewModelFactory(BoardRepository(retrofitService))).get(
                BoardViewModel::class.java
            )
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()

        return binding.root
    }

    override fun initData() {
        binding.recyclerView.adapter = adapter
        viewModel.boardList.observe(viewLifecycleOwner, Observer {status->
            when(status){
                is Resource.Loading -> {
                    binding.boardShimmer.startShimmer()
                }
                is Resource.Success -> {
                    status.data?.let {
                        adapter.setBoardList(it)
                        binding.scrollView.visibility = View.VISIBLE
                        binding.boardShimmer.stopShimmer()
                        binding.boardShimmer.visibility = View.GONE
                    }
                }
                is Resource.Error ->{
                    Toast.makeText(context,"Network Error",Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(context,"Unexpected Error",Toast.LENGTH_SHORT).show()
                }
            }


        })
        viewModel.errorMessage.observe(viewLifecycleOwner, Observer { })
        viewModel.getAllBoards()
    }

    override fun initListener() {
        adapter.setOnBoardItemClickListener(object : BoardAdapter.onBoardItemClickListener {
            override fun onItemClick(position: Int) {
                Toast.makeText(context, position.toString(), Toast.LENGTH_SHORT).show()
            }

        })
    }

}