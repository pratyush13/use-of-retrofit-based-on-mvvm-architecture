package com.example.instamoodindex.ui

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.instamoodindex.ViewModel.SuccessMetric.SuccessMetricRepository
import com.example.instamoodindex.ViewModel.SuccessMetric.SuccessMetricViewModel
import com.example.instamoodindex.ViewModel.SuccessMetric.SuccessMetricViewModelFactory
import com.example.instamoodindex.adapters.SucesssMetricAdapter
import com.example.instamoodindex.base.BaseFragment
import com.example.instamoodindex.databinding.FragmentSuccessMetricBinding
import com.example.instamoodindex.helper.GravitySnapHelper
import com.example.instamoodindex.retrofit.MockRetrofitService
import java.util.*


class SuccessMetricFragment : BaseFragment() {
    private lateinit var binding: FragmentSuccessMetricBinding
    private lateinit var viewModel: SuccessMetricViewModel
    private val retrofitService = MockRetrofitService.getInstance()
    private val adapter = SucesssMetricAdapter()
    private lateinit var layoutManager: LinearLayoutManager

    override fun initView(layoutInflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentSuccessMetricBinding.inflate(layoutInflater,container,false)
        viewModel =
            ViewModelProvider(this, SuccessMetricViewModelFactory(SuccessMetricRepository(retrofitService))).get(
                SuccessMetricViewModel::class.java
            )
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
        binding.rcvSuccessMetric.layoutManager = layoutManager
        autoScrollRecyclerView()
        return binding.root
    }

    override fun initData() {

        binding.rcvSuccessMetric.adapter = adapter
        viewModel.successMetricList.observe(viewLifecycleOwner, Observer {
            adapter.setSuccessMetricList(it)
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {  })
        viewModel.getAllSuccessMetric()
    }

    private fun autoScrollRecyclerView() {
        val linearSnapHelper = GravitySnapHelper(Gravity.START)
        linearSnapHelper.attachToRecyclerView(binding.rcvSuccessMetric)
        val timer = Timer()
        timer.schedule(object : TimerTask(){
            override fun run() {
                if (layoutManager.findLastCompletelyVisibleItemPosition() < (adapter.itemCount)-1)
                    layoutManager.smoothScrollToPosition(binding.rcvSuccessMetric, RecyclerView.State(),layoutManager.findLastCompletelyVisibleItemPosition()+2)
                else
                    layoutManager.smoothScrollToPosition(binding.rcvSuccessMetric,RecyclerView.State(),0)
            }

        },0,5000)

    }

    override fun initListener() {
        binding.rcvSuccessMetric.setOnTouchListener({ v, event -> true })
    }





}