package com.example.instamoodindex.ui

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.instamoodindex.MoodAdapter
import com.example.instamoodindex.ViewModel.Mood.MoodRepository
import com.example.instamoodindex.ViewModel.Mood.MoodViewModel
import com.example.instamoodindex.ViewModel.Mood.MoodViewModelFactory
import com.example.instamoodindex.base.BaseFragment
import com.example.instamoodindex.databinding.FragmentHowDoYouFeelBinding
import com.example.instamoodindex.retrofit.RetrofitService

class HowDoYouFeelFragment() : BaseFragment() {

    private lateinit var binding: FragmentHowDoYouFeelBinding
    private lateinit var viewModel: MoodViewModel
    private lateinit var sharedPreferences: SharedPreferences
    private val retrofitService = RetrofitService.getInstance()
    private val adapter = MoodAdapter()

    override fun initView(layoutInflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentHowDoYouFeelBinding.inflate(layoutInflater, container, false)
        viewModel =
            ViewModelProvider(this, MoodViewModelFactory(MoodRepository(retrofitService))).get(
                MoodViewModel::class.java
            )

        return binding.root
    }
    override fun initData() {
        sharedPreferences = requireActivity().getSharedPreferences("timePref", Context.MODE_PRIVATE)
        val lastMoodClickSecondsDiff =
            (System.currentTimeMillis() - sharedPreferences.getLong("moodClickTime", 0)) / 1000

        if (lastMoodClickSecondsDiff < 600) {
            Toast.makeText(this.context, "Not been 10 mins", Toast.LENGTH_LONG).show()
            this.view?.visibility = View.GONE
        }

        binding.recyclerView.adapter = adapter

        viewModel.moodList.observe(viewLifecycleOwner, Observer {
            Log.d("MainActivity", "onCreate: $it")
            adapter.setMoodList(it)
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, Observer { })
        viewModel.getAllMoods()

    }

    override fun initListener() {
        binding.closeButton.setOnClickListener {
            this.view?.visibility = View.GONE
        }

        adapter.setOnMoodItemClickListener(object : MoodAdapter.onMoodItemClickListener {
            override fun onItemClick(position: Int) {
                Toast.makeText(context, "Mood Index: $position", Toast.LENGTH_SHORT).show()
                val editor = sharedPreferences.edit()
                editor.apply {
                    editor.putLong("moodClickTime", System.currentTimeMillis())
                    apply()
                }
            }
        })
    }

}