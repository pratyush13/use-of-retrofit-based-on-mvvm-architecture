package com.example.instamoodindex.retrofit

import com.example.instamoodindex.ViewModel.SuccessMetric.SuccessMetric
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MockRetrofitService {

    @GET("api/successmetric")
    fun getAllSuccessMetric(): Call<List<SuccessMetric>>

    companion object {
        private var mockRetrofitService: MockRetrofitService? = null

        fun getInstance() : MockRetrofitService {

            if (mockRetrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://6272501525fed8fcb5f239db.mockapi.io")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                mockRetrofitService = retrofit.create(MockRetrofitService::class.java)
            }
            return mockRetrofitService!!
        }
    }
}