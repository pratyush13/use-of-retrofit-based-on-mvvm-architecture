package com.example.instamoodindex.retrofit

import com.example.instamoodindex.ViewModel.ChangeBoardClass.Board
import com.example.instamoodindex.ViewModel.Mood.Mood
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RetrofitService {

    @GET("api/moods/index")
    fun getAllMoods() : Call<List<Mood>>

    @GET("api/board")
    fun getAllBoards(): Call<List<Board>>

    companion object {

        private var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://staging.instapreps.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}