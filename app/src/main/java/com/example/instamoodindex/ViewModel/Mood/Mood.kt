package com.example.instamoodindex.ViewModel.Mood

data class Mood(
    val id: Int,
    val mood: String,
    val created_at: String,
    val updated_at: String
)