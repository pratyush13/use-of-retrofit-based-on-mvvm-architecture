package com.example.instamoodindex.ViewModel.SuccessMetric

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.instamoodindex.ViewModel.Mood.Mood
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SuccessMetricViewModel(private val repository: SuccessMetricRepository):ViewModel() {
    val successMetricList = MutableLiveData<List<SuccessMetric>>()
    val errorMessage = MutableLiveData<String>()

    fun getAllSuccessMetric(){
        val response = repository.getAllSuccessMetric()
        response.enqueue(object : Callback<List<SuccessMetric>>{
                override fun onResponse(call: Call<List<SuccessMetric>>, response: Response<List<SuccessMetric>>) {
                successMetricList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<SuccessMetric>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }

        })
    }
}