package com.example.instamoodindex.ViewModel.Mood

import com.example.instamoodindex.retrofit.RetrofitService

class MoodRepository constructor(private val retrofitService: RetrofitService) {
    fun getAllMoods() = retrofitService.getAllMoods()
}