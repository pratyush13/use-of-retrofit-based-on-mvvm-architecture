package com.example.instamoodindex.ViewModel.ChangeBoardClass

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.instamoodindex.helper.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BoardViewModel(private val repository: BoardRepository) : ViewModel() {
    val boardList = MutableLiveData<Resource<List<Board>>>()
    val errorMessage = MutableLiveData<String>()

    fun getAllBoards() {

        val response = repository.getAllBoards()
        response.enqueue(object : Callback<List<Board>> {
            override fun onFailure(call: Call<List<Board>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }

            override fun onResponse(call: Call<List<Board>>, response: Response<List<Board>>) {
                boardList.postValue(Resource.Success(response.body()!!))
            }
        })
    }


}