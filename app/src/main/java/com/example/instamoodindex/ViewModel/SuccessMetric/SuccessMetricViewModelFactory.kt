package com.example.instamoodindex.ViewModel.SuccessMetric

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SuccessMetricViewModelFactory constructor(private val repository: SuccessMetricRepository) : ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(SuccessMetricViewModel::class.java)) {
            SuccessMetricViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}