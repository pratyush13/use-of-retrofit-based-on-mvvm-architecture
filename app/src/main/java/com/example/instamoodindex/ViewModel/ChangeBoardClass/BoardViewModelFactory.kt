package com.example.instamoodindex.ViewModel.ChangeBoardClass

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class BoardViewModelFactory constructor(private val repository: BoardRepository) : ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(BoardViewModel::class.java)) {
            BoardViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }

}