package com.example.instamoodindex.ViewModel.Mood

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoodViewModel(private val repository: MoodRepository) : ViewModel() {
    val moodList = MutableLiveData<List<Mood>>()
    val errorMessage = MutableLiveData<String>()


    fun getAllMoods() {
        val response = repository.getAllMoods()
        response.enqueue(object : Callback<List<Mood>> {
            override fun onResponse(call: Call<List<Mood>>, response: Response<List<Mood>>) {
                moodList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<Mood>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}