package com.example.instamoodindex.ViewModel.SuccessMetric

import com.example.instamoodindex.retrofit.MockRetrofitService

class SuccessMetricRepository constructor(private val mockRetrofitService: MockRetrofitService){
    fun getAllSuccessMetric() = mockRetrofitService.getAllSuccessMetric()
}