package com.example.instamoodindex.ViewModel.ChangeBoardClass

data class Board(
    val id: Int,
    val board: String,
    val created_at: String,
    val updated_at: String,
    val priority: Int,
    val publish: Boolean,

)