package com.example.instamoodindex.ViewModel.SuccessMetric

data class SuccessMetric(
    val id: String,
    val name: String,
    val value: String
)