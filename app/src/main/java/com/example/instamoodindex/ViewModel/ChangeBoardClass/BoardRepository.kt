package com.example.instamoodindex.ViewModel.ChangeBoardClass

import com.example.instamoodindex.retrofit.RetrofitService

class BoardRepository constructor(private val retrofitService: RetrofitService) {
    fun getAllBoards() = retrofitService.getAllBoards()
}