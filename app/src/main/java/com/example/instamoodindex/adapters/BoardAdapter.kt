package com.example.instamoodindex.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.instamoodindex.ViewModel.ChangeBoardClass.Board
import com.example.instamoodindex.databinding.AdapterBoardBinding

class BoardAdapter: RecyclerView.Adapter<BoardViewHolder>() {
    private lateinit var boardListener: onBoardItemClickListener
    private var boards = mutableListOf<Board>()

    interface onBoardItemClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnBoardItemClickListener(listener: onBoardItemClickListener){
        boardListener = listener
    }

    fun setBoardList(boards: List<Board>){
        this.boards = boards.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterBoardBinding.inflate(inflater,parent,false)
        return BoardViewHolder(binding, boardListener)
    }

    override fun onBindViewHolder(holder: BoardViewHolder, position: Int) {
        val board = boards[position]
        holder.binding.boardButton.text = board.board
    }

    override fun getItemCount(): Int {
        return boards.size
    }
}

class BoardViewHolder(val binding: AdapterBoardBinding, listener: BoardAdapter.onBoardItemClickListener): RecyclerView.ViewHolder(binding.root){
    init {
        binding.boardButton.setOnClickListener{
            listener.onItemClick(adapterPosition)
        }
    }
}