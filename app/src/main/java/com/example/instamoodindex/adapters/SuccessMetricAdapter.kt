package com.example.instamoodindex.adapters

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.instamoodindex.R
import com.example.instamoodindex.databinding.SuccessMetricItemBinding
import com.example.instamoodindex.ViewModel.SuccessMetric.SuccessMetric

@Suppress("DEPRECATION")
class SucesssMetricAdapter : RecyclerView.Adapter<SuccessMetricViewHolder>() {
    private var successMetricList = mutableListOf<SuccessMetric>()

    fun setSuccessMetricList(successMetricList: List<SuccessMetric>) {
        this.successMetricList = successMetricList.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SuccessMetricViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SuccessMetricItemBinding.inflate(inflater, parent, false)
        binding.cvSucceessMetricContainer.layoutParams = ViewGroup.LayoutParams((parent.measuredWidth*0.5).toInt(),ViewGroup.LayoutParams.MATCH_PARENT)
        return SuccessMetricViewHolder(binding)
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onBindViewHolder(holder: SuccessMetricViewHolder, position: Int) {
        val successMetric = successMetricList[position]

//        val wm: WindowManager = holder.itemView.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
//        val display = wm.defaultDisplay
//        //holder.binding.cvSucceessMetricContainer.layoutParams = RecyclerView.LayoutParams((display.width/2), 400)
        holder.binding.tvHeading.text = successMetric.value
        holder.binding.tvDescription.text = successMetric.name
        when(successMetric.id){
            "1" -> Glide.with(holder.itemView.context).load(R.drawable.location_anim)
                .into(holder.binding.ivBannerImage)
            "2" -> Glide.with(holder.itemView.context).load(R.drawable.confidence_anim)
                .into(holder.binding.ivBannerImage)
            "3" -> Glide.with(holder.itemView.context).load(R.drawable.question_anim)
                .into(holder.binding.ivBannerImage)
            "4" -> Glide.with(holder.itemView.context).load(R.drawable.rating_anim)
                .into(holder.binding.ivBannerImage)

        }

    }


    override fun getItemCount(): Int {
        return successMetricList.size
    }


}

class SuccessMetricViewHolder(val binding: SuccessMetricItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

}