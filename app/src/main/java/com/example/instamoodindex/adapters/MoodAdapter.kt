package com.example.instamoodindex

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.instamoodindex.ViewModel.Mood.Mood
import com.example.instamoodindex.databinding.AdapterMoodBinding

class MoodAdapter : RecyclerView.Adapter<MoodViewHolder>() {

    private lateinit var moodListener: onMoodItemClickListener
    private var moods = mutableListOf<Mood>()

    interface onMoodItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnMoodItemClickListener(listener: onMoodItemClickListener){
        moodListener = listener
    }

    fun setMoodList(moods: List<Mood>) {
        this.moods = moods.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoodViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterMoodBinding.inflate(inflater, parent, false)
        return MoodViewHolder(binding, moodListener)
    }

    override fun onBindViewHolder(holder: MoodViewHolder, position: Int) {
        val mood = moods[position]
        when (mood.mood) {
            "Good" -> Glide.with(holder.itemView.context).load(R.drawable.good_face)
                .into(holder.binding.moodImageView)
            "Bad" -> Glide.with(holder.itemView.context).load(R.drawable.bad_face)
                .into(holder.binding.moodImageView)
            "Neutral" -> Glide.with(holder.itemView.context).load(R.drawable.neutral_face)
                .into(holder.binding.moodImageView)
            "Confident" -> Glide.with(holder.itemView.context).load(R.drawable.confident_face)
                .into(holder.binding.moodImageView)
            "Awesome" -> Glide.with(holder.itemView.context).load(R.drawable.awesome_face)
                .into(holder.binding.moodImageView)
            "Sad" -> Glide.with(holder.itemView.context).load(R.drawable.sad_face)
                .into(holder.binding.moodImageView)
        }
    }

    override fun getItemCount(): Int {
        return moods.size
    }

}

class MoodViewHolder(val binding: AdapterMoodBinding, listener: MoodAdapter.onMoodItemClickListener) : RecyclerView.ViewHolder(binding.root) {
    init{
        binding.moodImageView.setOnClickListener {
            listener.onItemClick(adapterPosition)
        }
    }

}